# Grails Assessment - Oleksandr Kononov
## Task
The aim of this Grails assessment is to create a web-based Address Book application,
the application should be created though IntelliJ IDEA and a MySQL database should be utilised.

Create an address book application that stores all forms of contact information about a person,
the application should be able to do the following:
- Add/Remove/Edit a user
- List all information about a specific user
- List all users with the option to sort by last name or first name
- Bootstrap default users

### Application model
- It is important to pay attention to domain **constraints** and **relationships** in domains
_(where applicable)_.
- Constraints should be tested with unit tests.
- Methods relevant to domains should be placed within domains where necessary.
- When unit tests aren't possible, Grails integration tests should be written.

### Application view
- Use **scaffolding** where appropriate.
- Create GSPs (Groovy Server Pages) when more flexibility and/or customization is required.

## Running Address Book
- In the project root directory run `grails run-app` in the terminal
- In the browser, enter the following into the url `localhost:8080/addressbook`

## Testing
### Running all test
- In the project root directory run `grails test-app` in the terminal
- (Reports in HTML can be found at `target/test-reports/html/all.html`)

### Running only unit tests
- In the project root directory run `grails test-app -unit` in the terminal

### Running only integration tests
- In the project root directory run `grails test-app -integration` in the terminal
