import com.errigal.oleks.Person

class BootStrap {

    def init = { servletContext ->
        environments {
            development {
                if (!Person.count()) {
                    createSampleData()
                }
            }
            test {
                if (!Person.count()) {
                    createSampleData()
                }
            }
        }
    }

    private createSampleData(){
        println "Creating sample data (persons)"

        new Person(firstName: "Test",
          lastName: "Tester",
          phone: "1234567890",
          email: "test@test.com",
          address: "NA").save(failOnError: true)

        new Person(firstName: "ABC",
          lastName: "ZYX",
          phone: "0987654321",
          email: "testabc@test.com",
          address: "NA").save(failOnError: true)

        new Person(firstName: "ZYX",
          lastName: "ABC",
          phone: "1234509876",
          email: "test3@test.com",
          address: "NA").save(failOnError: true)
    }

    def destroy = {
    }
}
