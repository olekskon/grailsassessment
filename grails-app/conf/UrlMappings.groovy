import com.errigal.oleks.Person

class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		i//"/"(view:"/index")
		"/"(controller: "Person", action: "index")
		"500"(view:'/error')
	}
}
