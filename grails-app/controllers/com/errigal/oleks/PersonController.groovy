package com.errigal.oleks

import org.springframework.dao.DataIntegrityViolationException

class PersonController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def personService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [personInstanceList: Person.list(params), personInstanceTotal: Person.count()]
    }

    def create() {
        [personInstance: new Person(params)]
    }

    def save(String firstName, String lastName, String phone, String email, String address) {
        try{
          def newPerson = personService.createPerson(firstName, lastName, phone, email, address)
          flash.message = "Added new person: ${newPerson.firstName} ${newPerson.lastName}"
        } catch (PersonException ppe) {
            flash.message = ppe.message
            redirect(action: 'create')
            return
        }
        redirect(action: 'list')
    }

    def show(Long id) {
        def personInstance = Person.get(id)
        if (!personInstance) {
            flash.message = "This person does not exist"
            redirect(action: "list")
            return
        }

        [personInstance: personInstance]
    }

    def edit(Long id) {
        def personInstance = Person.get(id)
        if (!personInstance) {
            flash.message = "This person does not exist"
            redirect(action: "list")
            return
        }

        [personInstance: personInstance]
    }

    def update(Long id, Long version) {
        def personInstance = Person.get(id)
        if (!personInstance) {
            flash.message = "This person does not exist"
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (personInstance.version > version) {
                personInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'person.label', default: 'Person')] as Object[],
                          "Another user has updated this Person while you were editing")
                render(view: "edit", model: [personInstance: personInstance])
                return
            }
        }

        personInstance.properties = params

        if (!personInstance.save(flush: true)) {
            render(view: "edit", model: [personInstance: personInstance])
            return
        }

        flash.message = "Person successfully updated"
        redirect(action: "show", id: personInstance.id)
    }

    def delete(Long id) {
        def personInstance = Person.get(id)
        if (!personInstance) {
            flash.message = "This person does not exist"
            redirect(action: "list")
            return
        }

        try {
            personInstance.delete(flush: true)
            flash.message = "Successfully deleted person"
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = "Could not delete person"
            redirect(action: "show", id: id)
        }
    }
}
