package com.errigal.oleks

class Person {
    String firstName
    String lastName
    String phone
    String email
    String address

    static constraints = {
        firstName(nullable: false, blank: false)
        lastName(nullable: false, blank: false)
        phone(nullable: false, blank: false, validator: { it ==~ /\+?\d{10,}/ })
        email(nullable: false, blank: false, email: true)
        address(nullable: false, blank: false)
    }

    String toString(){
        return "$firstName $lastName"
    }
}
