package com.errigal.oleks

class PersonException extends RuntimeException{
  String message
}

class PersonService {

  def createPerson(String firstName, String lastName, String phone, String email, String address){
    Person p = new Person(firstName: firstName,
                          lastName: lastName,
                          phone: phone,
                          email: email,
                          address: address)
    if (p.validate() && p.save()) {
      return p
    } else {
      throw new PersonException(message: "Invalid parameters for person")
    }
  }

}
