/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-19
 */

import com.errigal.oleks.Person
import spock.lang.Specification

class PersonIntegrationSpec extends Specification{

  def "Saving a person to the database"(){
    given: "A new person"
    def p = new Person(firstName: "TesterInt",
                       lastName: "TestingInt",
                       phone: "1234567890",
                       email: "test@test.com",
                       address: "none")

    when: "the person is saved"
    p.save()

    then: "it saved successfully to the database"
    p.errors.errorCount == 0
    p.id != null
    Person.get(p.id).firstName == p.firstName
    Person.get(p.id).lastName == p.lastName
    Person.get(p.id).phone == p.phone
    Person.get(p.id).email == p.email
    Person.get(p.id).address == p.address
  }

  def "Update a saved person changes its properties"() {
    given: "an existing person"
    def p = new Person(firstName: "TesterInt",
      lastName: "TestingInt",
      phone: "1234567890",
      email: "test@test.com",
      address: "none")
    p.save(failOnError: true)

    when: "a property is changed"
    def foundPerson = Person.get(p.id)
    foundPerson.firstName = "testertesterInt"
    foundPerson.save(failOnError: true)

    then: "the change is reflected in the database"
    Person.get(p.id).firstName == "testertesterInt"
  }

  def "Deleting an existing person removes it from the database"() {
    given: "an existing person"
    def p = new Person(firstName: "TesterInt",
      lastName: "TestingInt",
      phone: "1234567890",
      email: "test@test.com",
      address: "none")
    p.save(failOnError: true)

    when: "the person is deleted"
    def foundPerson = Person.get(p.id)
    foundPerson.delete(flush: true)

    then: "the person is removed from the database"
    !Person.exists(foundPerson.id)
  }

  def "Saving a person with invalid properties causes an error"() {
    given: "a person which fails several field validations"
    def p = new Person(firstName: "TesterInt",
      lastName: "TestingInt",
      phone: "123456",
      email: "test.com",
      address: "none")

    when: "the person is validated"
    p.validate()

    then: "the person has errors"
    p.hasErrors()
  }

  def "Recovering from a failed save by fixing invalid properties"(){
    given: "A person that has invalid properties"
    def p = new Person(firstName: "TesterInt",
      lastName: "TestingInt",
      phone: "123456",
      email: "test.com",
      address: "none")
    assert p.save() == null
    assert p.hasErrors()

    when: "fixing the invalid properties"
    p.phone = "1234567890"
    p.email = "proper_email@email.com"
    p.validate()

    then: "the person saves and validates"
    !p.hasErrors()
    p.save()
  }
}
