/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-19
 */

package com.errigal.oleks

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(PersonController)
@Mock([Person, PersonService])
class PersonControllerSpec extends Specification{

  def "Listing person list"(){
    when: "listing person list"
    def params = controller.list(10)

    then: "person list is given"
    params.personInstanceList.size() == Person.count()
    params.personInstanceTotal == Person.count()
  }

  def "Create person with params"(){
    given: "params with person data for data binding"
    params.with {
      firstName = "TesterWithParams"
      lastName = "TestingWithParams"
      phone = "1234567890"
      email = "good_params@test.com"
      address = "none"
    }

    when: "creating a person"
    def result = controller.create()

    then: "receive created person"
    Person p = result.personInstance
    p.firstName == "TesterWithParams"
    p.lastName == "TestingWithParams"
    p.phone == "1234567890"
    p.email == "good_params@test.com"
    p.address == "none"
  }

  def "Saving a valid person"() {
    given: "valid parameters to a person"
    String firstName = "goodParamTester"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "current person count"
    int personCount = Person.count()

    when: "creating and saving a person"
    controller.save(firstName, lastName, phone, email, address)

    then: "person successfully created and saved user redirected to list action"
    Person.count() > personCount
    controller.flash.message == "Added new person: $firstName $lastName"
    controller.response.redirectedUrl == "/person/list"
  }

  def "Saving an invalid person"() {
    given: "invalid parameters to a person"
    String firstName = "goodParamTester"
    String lastName = ""
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = ""

    and: "current person count"
    int personCount = Person.count()

    when: "creating and saving a person"
    controller.save(firstName, lastName, phone, email, address)

    then: "person is not saved and user is redirected to create action"
    Person.count() == personCount
    controller.flash.message == "Invalid parameters for person"
    controller.response.redirectedUrl == "/person/create"
  }

  def "Showing an existing person"(){
    given: "an existing saved person"
    def p = new Person(firstName: "firstNameTest",
      lastName: "lastNameTest",
      phone: "1234567890",
      email: "test@test.com",
      address: "none").save(failOnError: true)

    when: "person is shown"
    def shownPerson = controller.show(p.id).personInstance

    then: "valid person is retrieved"
    p.firstName == shownPerson.firstName
    p.lastName == shownPerson.lastName
    p.phone == shownPerson.phone
    p.email == shownPerson.email
    p.address == shownPerson.address
  }

  def "Attempting to show non existent person"() {
    when: "attempting to show a non existent person"
    controller.show(10000)

    then: "no person is shown"
    controller.flash.message == "This person does not exist"
    controller.response.redirectedUrl == "/person/list"
  }

  def "Editing an existing person"(){
    given: "an existing saved person"
    def p = new Person(firstName: "firstNameTest",
      lastName: "lastNameTest",
      phone: "1234567890",
      email: "test@test.com",
      address: "none").save(failOnError: true)

    when: "editing a person"
    def editPerson = controller.edit(p.id).personInstance

    then: "valid person is retrieved for editing"
    p.firstName == editPerson.firstName
    p.lastName == editPerson.lastName
    p.phone == editPerson.phone
    p.email == editPerson.email
    p.address == editPerson.address
  }

  def "Attempting to edit non existent person"() {
    when: "attempting to edit a non existent person"
    controller.edit(10000)

    then: "no person is prepared for editing"
    controller.flash.message == "This person does not exist"
    controller.response.redirectedUrl == "/person/list"
  }

  def "Update an existing person"() {
    given: "an existing saved person"
    def p = new Person(firstName: "firstNameTest",
      lastName: "lastNameTest",
      phone: "1234567890",
      email: "test@test.com",
      address: "none").save(failOnError: true)

    and: "update params for person"
    params.with {
      firstName = "newFirstNameTest"
      email = "newtest@test.com"
    }

    when: "a person is updated"
    controller.update(p.id, p.version)

    then: "the person is successfully updated"
    def updatedPerson = Person.get(p.id)
    updatedPerson.firstName == "newFirstNameTest"
    updatedPerson.email == "newtest@test.com"
    controller.flash.message == "Person successfully updated"
    controller.response.redirectedUrl == "/person/show/${updatedPerson.id}"
  }

  def "Attempt to update a non existent person"() {
    when: "attempting to update a non existent person"
    controller.update(1000000,1)

    then: "failed to update a non existent person and redirected to list"
    controller.flash.message == "This person does not exist"
    controller.response.redirectedUrl == "/person/list"
  }

  def "Attempt to update a person with wrong version"() {
    given: "an existing saved person with new version"
    def p = new Person(firstName: "firstNameTest",
      lastName: "lastNameTest",
      phone: "1234567890",
      email: "test@test.com",
      address: "none").save(failOnError: true)
    p.version = 2
    p.save(failOnError: true)

    and: "update params for person"
    params.with {
      firstName = "badUpdateFirstName"
    }

    when: "attempting to update a person with wrong version"
    controller.update(p.id, 1)

    then:
    p.hasErrors()
    p.firstName != "badUpdateFirstName"
  }

  def "Delete a saved person"() {
    given: "an existing saved person"
    def p = new Person(firstName: "firstNameTest",
      lastName: "lastNameTest",
      phone: "1234567890",
      email: "test@test.com",
      address: "none").save(failOnError: true)

    and: "current count of persons with new saved person"
    int currentPersonCount = Person.count()

    when: "deleting a saved person"
    controller.delete(p.id)

    then: "person is deleted successfully"
    currentPersonCount > Person.count()
    !Person.exists(p.id)
    controller.flash.message == "Successfully deleted person"
    controller.response.redirectedUrl == "/person/list"
  }

  def "Attempt to delete a non existent person"() {
    given: "current person count"
    int currentPersonCount = Person.count()

    when: "attempting to delete a non existent person"
    controller.delete(100000)

    then: "fail to delete a non existent person"
    controller.flash.message == "This person does not exist"
    controller.response.redirectedUrl == "/person/list"
  }
}
