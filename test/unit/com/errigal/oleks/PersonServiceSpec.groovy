package com.errigal.oleks

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(PersonService)
@Mock([Person])
class PersonServiceSpec extends Specification {

  def "Valid person saved"() {
    given: "valid person parameters"
    String firstName = "goodParamTester"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "initial person count"
    int preAdditionCount = Person.count()

    when: "a new person is added"
    def newPerson = service.createPerson(firstName, lastName, phone, email, address)

    then: "the new person returned and added to the database"
    newPerson.firstName == firstName
    newPerson.lastName == lastName
    newPerson.phone == phone
    newPerson.email == email
    newPerson.address == address
    Person.count() - preAdditionCount == 1
  }

  def "Invalid person phone parameter too short"(){
    given: "invalid person phone parameter too short"
    String firstName = "goodTestParam"
    String lastName = "goodTestParam"
    String phone = "1234567"
    String email = "good_email@email.com"
    String address = "none"

    and: "initial person count"
    int initialCount = Person.count()

    when: "an invalid person addition is attempted"
    def newPerson = service.createPerson(firstName, lastName, phone, email, address)

    then: "an exception is thrown and no person is saved"
    thrown(PersonException)
    Person.count() == initialCount
  }

  def "Invalid person phone parameter non digit"(){
    given: "invalid person phone parameter non digit"
    String firstName = "goodTestParam"
    String lastName = "goodTestParam"
    String phone = "1234567fffff"
    String email = "good_email@email.com"
    String address = "none"

    and: "initial person count"
    int initialCount = Person.count()

    when: "an invalid person addition is attempted"
    def newPerson = service.createPerson(firstName, lastName, phone, email, address)

    then: "an exception is thrown and no person is saved"
    thrown(PersonException)
    Person.count() == initialCount
  }

  def "Invalid person email format parameter"() {
    given: "invalid person blank parameters"
    String firstName = "GoodName"
    String lastName = "GoodTester"
    String phone = "1234567890"
    String email = "not_an_email"
    String address = "none"

    and: "initial person count"
    int initialCount = Person.count()

    when: "an invalid person addition is attempted"
    def newPerson = service.createPerson(firstName, lastName, phone, email, address)

    then: "an exception is thrown and no person is saved"
    thrown(PersonException)
    Person.count() == initialCount
  }

  def "Invalid person blank parameter"() {
    given: "invalid person blank parameters"
    String firstName = ""
    String lastName = ""
    String phone = ""
    String email = ""
    String address = ""

    and: "initial person count"
    int initialCount = Person.count()

    when: "an invalid person addition is attempted"
    def newPerson = service.createPerson(firstName, lastName, phone, email, address)

    then: "an exception is thrown and no person is saved"
    thrown(PersonException)
    Person.count() == initialCount
  }

  def "Invalid person null parameters"() {
    given: "invalid person null parameters"
    String firstName = null
    String lastName = null
    String phone = null
    String email = null
    String address = null

    and: "initial person count"
    int initialCount = Person.count()

    when: "an invalid person addition is attempted"
    def newPerson = service.createPerson(firstName, lastName, phone, email, address)

    then: "an exception is thrown and no person is saved"
    thrown(PersonException)
    Person.count() == initialCount
  }
}
