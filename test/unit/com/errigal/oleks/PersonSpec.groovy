/***************************************************************
 * Copyright (c) 2019 Errigal Inc.
 *
 * This software is the confidential and proprietary information
 * of Errigal, Inc.  You shall not disclose such confidential
 * information and shall use it only in accordance with the
 * license agreement you entered into with Errigal.
 *
 ***************************************************************/
/**
 *
 * User: Oleksandr Kononov
 * Date: 2019-08-19
 */

package com.errigal.oleks

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(Person)
@Mock([Person])
class PersonSpec extends Specification {

  def "Valid person object creation"(){
    given: "valid parameters"
    String firstName = "goodParamTester"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    when: "person is created"
    def p = new Person(firstName: firstName,
                       lastName: lastName,
                       phone: phone,
                       email: email,
                       address: address)
    p.validate()

    then: "person has no errors"
    !p.hasErrors()
  }

  def "Invalid first name constraints"() {
    given: "some valid parameters"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "invalid first name"
    String firstName = ""

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Null first name constraints"() {
    given: "some valid parameters"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "null first name parameter"
    String firstName = null

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid last name constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "invalid last name parameter"
    String lastName = ""

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Null last name constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "null last name parameter"
    String lastName = null

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid phone constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "invalid phone parameter"
    String phone = ""

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Null phone constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "null phone parameter"
    String phone = null

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid phone constraints non digit"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "non digit phone parameter"
    String phone = "qwertyuiopsdf"

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid phone constraints too short"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String email = "good_param_test@test.com"
    String address = "none"

    and: "too short phone parameter"
    String phone = "123456789"

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid email constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String address = "none"

    and: "invalid email parameter"
    String email = ""

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Null email constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String address = "none"

    and: "null email parameter"
    String email = null

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid email constraints no @"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String address = "none"

    and: "no @ email parameter"
    String email = "bad_email.com"

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid email constraints no fullstop"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String address = "none"

    and: "no fullstop email parameter"
    String email = "bad_email@com"

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Invalid address constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"

    and: "invalid address parameter"
    String address = ""

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }

  def "Null address constraints"() {
    given: "some valid parameters"
    String firstName = "goodParamTesting"
    String lastName = "goodParamTesting"
    String phone = "1234567890"
    String email = "good_param_test@test.com"

    and: "null address parameter"
    String address = null

    when: "person is created"
    def p = new Person(firstName: firstName,
      lastName: lastName,
      phone: phone,
      email: email,
      address: address)
    p.validate()

    then: "person has errors"
    p.hasErrors()
  }
}
